!************************************************************
! copyright (C) 2014 Dinesh Kumar dkumar@buffalo.edu
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License
! as published by the Free Software Foundation; either version 2
! of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
!***********************************************************

subroutine vel_update (dt)
    use particles
    use properties

    real(8), intent(in) :: dt

    ! local variables
    integer ::i, j, k
    real(8), allocatable :: unew(:)
    real(8) :: xi, xj, hi, hj, voli, volj
    real(8) :: a, b, vijhi, vijhj, s
    real(8) :: uleft(3),  uright(3), pstar, ustar
    real(8) :: dwi, dwj, dwdx, rhs
    real(8) :: us, ps
 
    allocate (unew(np))
    do 110 i = 1, np
    if (.not.parts(i)%ghost) then
    xi = parts(i)%x
    hi = parts(i)%h
    rhs = 0.d0
    do 100 j = 1, np
    if (i.eq.j) cycle
    xj = parts(j)%x
    hj = parts(j)%h

    if (abs(xi - xj) .le. 4.25d0 * hi ) then
    voli = 1.d0 / parts(i)%d
    volj = 1.d0 / parts(j)%d
            
    ! polynomial density
    a = (voli - volj) / (xi - xj)
    b = (voli + volj) / 2.d0
        
    ! 1 / (density)^2
    vijhi = 0.25d0 * (hi*a)**2 + b**2
    vijhj = 0.25d0 * (hj*a)**2 + b**2

    ! interface position for for RP
    s = (0.5d0 * hi**2 * a * b)/vijhi

    ! left states
    uleft(1) = parts(j)%d
    uleft(2) = parts(j)%u
    uleft(3) = parts(j)%p

    ! right states
    uright(1) = parts(i)%d
    uright(2) = parts(i)%u
    uright(3) = parts(i)%p

    ! setup the Riemann  problem using projections
    call muscl_projection (parts(j), parts(i), uleft, uright, s, dt)

    ! solve the Riemann Problem
    call riemann_solver (uleft, uright, ustar, pstar)
    !call riemannsolve (uleft, uright, ustar, pstar)

    if (isnan(pstar)) then
    write (0, *) "Error: pstar = NaN"
    stop
    endif
 
    dwi = dwdx (xi, xj, sqrt2*hi)
    dwj = dwdx (xi, xj, sqrt2*hj)
    rhs = rhs - mass*pstar*(vijhi*dwi + vijhj*dwj)
    endif
    100 continue

    ! update velocity
    unew(i) = parts(i)%u + (dt * rhs)
    endif
    110 continue

    do 120 i = 1, np
    parts(i)%uold = parts(i)%u
    parts(i)%u = unew(i)
    !print *, unew(i)
    120 continue

    deallocate(unew)
    900 format(I5, I5, 4F6.4)

end subroutine vel_update

subroutine muscl_projection (pal, par, uleft, uright, s_star, dt)
    use particles
    use properties
    use thermodynamics

    implicit none
    real (8), intent (inout) :: uleft(3), uright(3)
    real (8), intent (in) :: s_star, dt
    type (particle), intent (in) :: pal, par
    
    integer :: i
    real(8) :: sl, sr, s0, dt2, cl, cr, ds
    real(8) :: ul(3), ur(3), du(3)
    real(8) :: dudx1(3), dudx2(3), duds(3), tmp1, tmp2

    dt2 = 0.5d0 * dt

    ! celerity
    cl = dsqrt(g * pal%p / pal%d)
    cr = dsqrt(g * par%p / par%d)

    ds = par%x - pal%x

    if (order .eq. 2) then
    ! coordinate system
    s0 = 0.5d0 * (pal%x + par%x)
    sl = pal%x - s0
    sr = par%x - s0

    du(1) = (par%d - pal%d)
    du(2) = (par%u - pal%u)
    du(3) = (par%p - pal%p)

    ! gradients
    duds(1) = du(1)/ds
    duds(2) = du(2)/ds
    duds(3) = du(3)/ds

    !do 20 i = 1, 3
    !dudx1(i) = par%dudx(i)
    !tmp1 = dudx1(i) * duds(i)
    !tmp2 = dudx1(i) + duds(i)
    !if (tmp1 .ge. 0.d0 .and. dabs(tmp2) .gt. 0.d0) then
    !    duds(i) = 2.d0 * tmp1 / tmp2
    !else
    !    duds(i) = 0.d0
    !endif
    !20 continue

    ! left side
    ul(1) = uleft(1) + duds(1) * (s_star - cl * dt2 - sl)
    ul(2) = uleft(2) + duds(2) * (s_star - cl * dt2 - sl)
    ul(3) = uleft(3) + duds(3) * (s_star - cl * dt2 - sl)
    
    ! right side
    ur(1) = uright(1) + duds(1) * (s_star + cl * dt2 - sr)
    ur(2) = uright(2) + duds(2) * (s_star + cl * dt2 - sr)
    ur(3) = uright(3) + duds(3) * (s_star + cl * dt2 - sr)

    ! update second-order approximations
    do 40 i = 1, 3
    uleft(i) = ul(i)
    uright(i) = ur(i)
    40 continue
    endif

end subroutine muscl_projection 
