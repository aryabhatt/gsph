!************************************************************
! copyright (C) 2014 Dinesh Kumar dkumar@buffalo.edu
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License
! as published by the Free Software Foundation; either version 2
! of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
!***********************************************************


subroutine calc_gradients ()
    use particles

    implicit none
    integer i, j, k
    real(8) :: dsum(3)
    real(8) :: hi, xi, xj
    real(8) :: dwdx, dwi, dxdwi

    do 100 i = 1, np
    if (.not.parts(i)%ghost) then
    hi = parts(i)%h
    xi = parts(i)%x
    dsum (:) = 0.d0
    do 90 j = 1, np
    ! skip if i = j, to avoid divide by 0
    if (i .eq. j) cycle

    xj = parts(j)%x
    if (dabs(xi - xj) .lt. 3.d0 * hi) then
    dwi = dwdx(xi, xj, hi)
    dxdwi = (xi - xj) * dwi
    dsum(1) = dsum(1) + ((parts(i)%d - parts(j)%d)*dwi)/dxdwi
    dsum(2) = dsum(2) + ((parts(i)%u - parts(j)%u)*dwi)/dxdwi
    dsum(3) = dsum(3) + ((parts(i)%p - parts(j)%p)*dwi)/dxdwi
    endif
    90 continue
    parts(i)%dudx(1) = dsum(1)
    parts(i)%dudx(2) = dsum(2)
    parts(i)%dudx(3) = dsum(3)
    endif
    100 continue
end subroutine calc_gradients
