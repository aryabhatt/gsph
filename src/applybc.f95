!************************************************************
! copyright (C) 2014  Dinesh Kumar (dkumar@buffalo.edu)
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License
! as published by the Free Software Foundation; either version 2
! of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
!***********************************************************

subroutine update_ghost_density ()
    use particles
    use properties

    implicit none
    integer :: i, j, k
    real(8) :: xref, hi, xj, sumd
    real(8) :: weight

    ! update left-side
    do 20 i = 1, 7
    if (parts(i)%ghost) then
    sumd = 0.d0
    xref = bnd(1) + (bnd(1) - parts(i)%x)
    hi = parts(i)%h
    do 10 j = 1, np/2
    xj = parts(j)%x
    if (dabs(xref - xj) .lt. 3.d0 * hi) then
    sumd = sumd + mass * weight (xref, xj, hi)
    endif
    10 continue
    parts(i)%d = sumd
    endif
    20 continue
   
    ! update right-side
    do 40 i = np - 7, np
    if (parts(i)%ghost) then
    sumd = 0.d0
    xref = bnd(2) + (bnd(2) - parts(i)%x)
    hi = parts(i)%h
    do 30 j = np/2 , np
    xj = parts(j)%x
    if (dabs(xref - xj) .lt. 3.d0 * hi) then
    sumd = sumd + mass * weight (xref, xj, hi)
    endif
    30 continue
    parts(i)%d = sumd
    endif
    40 continue

end subroutine update_ghost_density

! update smoothing length for ghosts    
subroutine update_ghost_smlen 
    use particles

    implicit none
    integer i, j, k

    ! find first real particle on left
    do 10 i = 1, np
    if (.not.parts(i)%ghost) then
    j = i
    exit
    endif
    10 continue
   
    ! update leftside
    do 20 i = 1, j-1
    parts(i)%h = parts(j)%h
    20 continue

    ! find first real particle on right
    do 30 i = np, j, -1
    if (.not.parts(i)%ghost) then
    k = i
    exit
    endif
    30 continue

    ! update right side
    do 40 i = k+1, np
    parts(i)%h = parts(k)%h
    40 continue
end subroutine update_ghost_smlen

! update velocity for ghosts
subroutine update_ghost_vel
    use particles
    use properties
    implicit none
    integer :: i, j
    real(8) :: xref, xj, num, den, wi, hi
    real(8) :: weight

    ! left side
    do 20 i = 1, 7
    if (parts(i)%ghost) then
    num = 0.d0
    den = 0.d0
    xref = bnd(1) + (bnd(1) - parts(i)%x)
    hi = parts(i)%h
    do 10 j = 1, np / 2
    if (.not.parts(j)%ghost) then
    xj = parts(j)%x
    if  (dabs(xref - xj) .lt. 3.0 * hi) then
    wi = weight (xref, xj, hi)
    den = den + mass/parts(j)%d * wi
    num = num + parts(j)%u * mass/parts(j)%d * wi 
    endif
    endif
    10 continue
    parts(i)%u = -num/den
    endif
    20 continue

    ! right side
    do 40 i = np-7, np
    if (parts(i)%ghost) then
    num = 0.d0
    den = 0.d0
    xref = bnd(2) + (bnd(2) - parts(i)%x)
    hi = parts(i)%h
    do 30 j = np / 2, np
    if (.not.parts(j)%ghost) then
    xj = parts(j)%x
    if  (dabs(xref - xj) .lt. 3.0 * hi) then
    wi = weight (xref, xj, hi)
    den = den + mass/parts(j)%d * wi
    num = num + parts(j)%u * mass/parts(j)%d * wi 
    endif
    endif
    30 continue
    parts(i)%u = -num/den
    endif
    40 continue
end subroutine update_ghost_vel

! update pressure for ghosts
subroutine update_ghost_pres
    use particles
    use properties
    implicit none
    integer :: i, j
    real(8) :: xref, xj, num, den, wi, hi
    real(8) :: weight

    ! left side
    do 20 i = 1, 7
    if (parts(i)%ghost) then
    num = 0.d0
    den = 0.d0
    xref = bnd(1) + (bnd(1) - parts(i)%x)
    hi = parts(i)%h
    do 10 j = 1, np / 2
    if (.not.parts(j)%ghost) then
    xj = parts(j)%x
    if  (dabs(xref - xj) .lt. 3.0 * hi) then
    wi = weight (xref, xj, hi)
    den = den + mass/parts(j)%d * wi
    num = num + parts(j)%p * mass/parts(j)%d * wi 
    endif
    endif
    10 continue
    parts(i)%u = -num/den
    endif
    20 continue

    ! right side
    do 40 i = np-7, np
    if (parts(i)%ghost) then
    num = 0.d0
    den = 0.d0
    xref = bnd(2) + (bnd(2) - parts(i)%x)
    hi = parts(i)%h
    do 30 j = np / 2, np
    if (.not.parts(j)%ghost) then
    xj = parts(j)%x
    if  (dabs(xref - xj) .lt. 3.0 * hi) then
    wi = weight (xref, xj, hi)
    den = den + mass/parts(j)%d * wi
    num = num + parts(j)%p * mass/parts(j)%d * wi 
    endif
    endif
    30 continue
    parts(i)%u = -num/den
    endif
    40 continue
end subroutine update_ghost_pres
