
!  Subroutine SOLVER_EXACT:
!
subroutine riemann_solver (uleft, uright, ustar, pstar)

  use thermodynamics

  implicit none

  real(8), intent(in) :: uleft(3), uright(3)
  real(8), intent(out) :: ustar, pstar
  real(8) :: dl, ul, pl, dr, ur, pr
  real(8) :: cl, cr, wl, wr
  real(8) :: p, ci, ps, cs, vc, u, d, wave
  real(8), parameter :: pcav = 0.001
  integer  :: n, itermax,itermax2

  itermax      =  50
  itermax2     = 100

  ! primitives
  dl = uleft(1)
  ul = uleft(2)
  pl = uleft(3)

  dr = uright(1)
  ur = uright(2)
  pr = uright(3)

  ! Lagrangian sound speed
  cr =  sqrt(g*pr*dr)
  cl =  sqrt(g*pl*dl)
  wr =  cr
  wl = -cl
  ! pressure at the interface
  p  = (wr*pl-wl*pr+wr*wl*(ur-ul))/(wr-wl)
  if(p < pcav)then
     p = pcav
  endif
  if((p < pl).and.(p < pr))then
     cr = cr/dr
     cl = cl/dl
     ci = cr/pr**g4
     p  = (0.5*g1*(ul-ur)+cr+cl)/(ci+cl/pl**g4)
     if(p < pcav)then
        p = pcav
     else
        p = p**g5
     endif
     wr =  cr*dr*wave(p,pr)
     wl = -cl*dl*wave(p,pl)
  else
     ps = p
     wr =  cr*wave(p,pr)
     wl = -cl*wave(p,pl)
     if((abs(p/pl - 1.d0) >= 0.01).or.(abs(p/pr - 1.d0) >= 0.01))then
        n = 0
  101   continue
        p = (wr*pl-wl*pr+wl*wr*(ur-ul))/(wr-wl)
        if(p < pcav)then
           p = pcav
           goto 102
        endif
        wr =  cr*wave(p,pr)
        wl = -cl*wave(p,pl)
        if(abs((p-ps)/p) >= 1.0e-04)then
           ps = p
           n  = n + 1
           if(n > itermax) write(*,*)'n',n
           ! added to cleanly exit code
           if(n > itermax2)then
              write (*,*) 'convergence failure in riemann solver'
           endif
           goto 101
        endif
     endif
  102 continue
  endif

  ! velocity at the interface
  ustar = (wr*ur-wl*ul-(pr-pl))/(wr-wl)
  pstar = p
end subroutine riemann_solver

!############################################################################################
subroutine riemannsolve (uleft, uright, ustar, pstar)
    use thermodynamics

    real(8), intent(in) :: uleft(3), uright(3)
    real(8), intent(out) :: ustar, pstar
    real(8) :: wave, dpdv

    integer :: iter, maxiter
    real(8), parameter :: pcav = 0.001
    real(8) :: dl, ul, pl, dr, ur, pr
    real(8) :: cl, cr, wl, wr, u, p, pold
    real(8) :: zl, zr, usl, usr
    

    maxiter = 50

    ! primitives
    dl = uleft(1)
    ul = uleft(2)
    pl = uleft(3)

    dr = uright(1)
    ur = uright(2)
    pr = uright(3)

    ! Lagrangian sound speed and wavespeeds
    cr =  sqrt(g*pr*dr)
    cl =  sqrt(g*pl*dl)
    wr =  cr
    wl = -cl
    usr = ur
    usl = ul

    ! pressure at the interface
    p  = (wr*pl-wl*pr+wr*wl*(ur-ul))/(wr-wl)
    if(p < pcav)then
        p = pcav
    endif

    ! iterate to search for pstar and ustar
    do 10 iter = 1, maxiter
    ! save prev 
    pold = p
    ! left waves
    wl = wave (p, pl)
    zl = dpdv (p, pl, cl, wl)
    
    !right waves
    wr = wave (p, pr)
    zr = dpdv (p, pr, cr, wr)

    p = pold - zl * zr * (usl - usr)
    if (p .lt. pcav) then
    print *, 'Error: Riemann Solver failed'
    stop
    endif

    usl = ul + (p - pl) / wl
    usr = ur - (p - pr) / wr
    u =  ( zl * ul + zr * ur)/(zl + zr)

    if (dabs(p / pold) < 0.001) then
    exit
    endif
    
    10 continue
    
    ustar = u
    pstar = p 

end subroutine riemannsolve
!############################################################################################

!  Function WAVE:
!
function wave(ps ,p)
  use thermodynamics

  implicit none

  real(8), intent(in) :: p, ps
  real(8) ::  x, wave

  x = ps / p
  if(abs(x-1.d0) < 1.0e-03)then
     wave = 1.d0 + 0.5*g3*(x-1.d0)
     return
  elseif(ps .ge. p)then
     wave = sqrt(1.d0+g3*(x-1.d0))
     return
  else
     wave = g4*(1.d0-x)/(1.d0-x**g4)
     return
  endif

end function wave


!############################################################################################

function dpdv (ps, p, c, w)
    use thermodynamics

    implicit none
    real(8), intent (in) :: ps, p, c, w
    real(8) :: x, dpdv

    x = ps / p
    if ( ps .ge. p ) then
        dpdv = 2.d0 * w**3 / (w**2 + c**2)
    else
        dpdv = c * x**(1.d0 - g4)
    endif
end function dpdv
