! Gaussian kernel function
function weight (xi, xj, h)
    use properties
    implicit none
    real(8), intent (in) :: xi, xj, h
    real(8) :: s, weight

    s = (xi - xj) / h
    if (dabs(s) .ge. 3.d0) then
    weight = 0.d0
    else
    weight = inv_sqrt_pi / h * dexp (- s**2)
    endif
end function weight

! derivative of kernel function
function dwdx (xi, xj, h)
    use properties
    implicit none
    real(8), intent (in) :: xi, xj, h
    real(8) :: s, dwdx, weight

    s = (xi - xj) / h
    if (dabs(s) .ge. 3.d0) then
    dwdx = 0.d0
    else
    dwdx = -2.d0 * s / h * weight (xi, xj, h)
    endif
end function dwdx

! calculate timestep
function timestep () 
    use thermodynamics
    use particles

    implicit none
    real(8) :: timestep
    integer :: i
    real(8) :: eig, dt, dt2, c
    
    dt = 10000.d0
    do 50 i = 1, np
    c = dsqrt(g * parts(i)%p / parts(i)%d)
    eig = dabs (parts(i)%u) + c 
    dt2 = parts(i)%h / eig 
    if (dt2 .lt. dt) then
    dt = dt2
    endif
    50 continue
    timestep = 0.25d0 * dt
end function timestep 

! update density
subroutine density_update
    use particles
    use properties

    implicit none 
    integer :: i, j, jbeg, jend
    real(8) :: xi, xj, hi, hj, sumd
    real(8) :: wi, wj
    real(8) :: weight


    do 100 i = 1, np
    if (.not.parts(i)%ghost) then
    xi = parts(i)%x
    hi = parts(i)%h
    sumd = 0.d0
    do 50 j = 1, np
    xj = parts(j)%x
    hj = parts(j)%h
    if (dabs (xi - xj)/hi .le. 3.d0 ) then
    wi = weight (xi, xj, hi)
    wj = weight (xi, xj, hj)
    sumd = sumd + 0.5d0 * mass * (wi + wj)
    !print *, sumd
    endif
    50 continue
    parts(i)%d = sumd
    endif
    100 continue
end subroutine density_update


!
! move particles
! 
subroutine move_particles (dt)
    use particles
    use properties

    implicit none
    real(8), intent(in) :: dt
    real(8) :: weight, xi, xj, hi
    real(8) :: dj, vj, vs
    integer i, j

    do 20 i = 1, np
    if (.not.parts(i)%ghost) then
    vs = 0.d0
    do 15 j = i - 4, i + 4
    xi = parts(i)%x
    xj = parts(j)%x
    hi = parts(i)%h
    dj = parts(j)%d
    vj = parts(j)%u
    vs = vs + mass * vj * weight (xi, xj, hi) / dj
    15 continue
    if ( vs .lt. 0.001 ) vs = 0.d0
    parts(i)%x = parts(i)%x + dt * 0.5d0 *  (vs + parts(i)%u)
    endif
    20 continue
end subroutine move_particles

!
! pressure update
!
subroutine pressure_update
    use particles
    use properties
    use thermodynamics

    implicit none
    integer :: i
    do 20 i = 1, np
    !if (.not.parts(i)%ghost) then
    !parts(i)%p = parts(i)%d * parts(i)%e * (g - 1.d0)
    parts(i)%p = pconst * parts(i)%d**g
    !end if
    20 continue
end subroutine pressure_update


!
! adapt smoothing length
!
subroutine adapt_smlen
    use particles
    use properties

    implicit none
    real(8) :: xi, xj, hi, hj, smden
    real(8) :: weight, wi, wj
    integer :: i, j

    do 20 i = 1, np
    if (.not.parts(i)%ghost ) then
    smden = 0.d0
    do 10 j = 1, np
    xi = parts(i)%x
    xj = parts(j)%x
    hi = 2.d0 * parts(i)%h
    wi = weight (xi, xj,  hi)
    smden = smden + mass * wi
    10 continue
    parts(i)%h = mass / smden
    endif
    20 continue
end subroutine adapt_smlen

!
! write output 
!
subroutine write_output ( num )
    use particles
    implicit none
    integer, intent(in) :: num
    character(len=20) :: fname
    character(len=3) :: n
    integer :: i, io 

    write (n, 800) num
    fname = 'output'//trim(n)//'.csv'
    io = 13 + num

    open (unit = io, file = fname, status = 'replace', action = 'write')
    do 20 i = 1, np
    if (.not.parts(i)%ghost) then
    write (io, 900) parts(i)%x, parts(i)%d, parts(i)%u
    endif
    20 continue

    close (io)
    800 format (I3.3) 
    900 format (F15.8', 'F15.8', 'F15.8)
end subroutine write_output
