subroutine initialize (exper)
    use particles
    use properties

    implicit none
    integer, intent (in) :: exper

    integer :: i, j, k
    integer :: ngl, ngr, nl, nr
    real(8) :: dxl, dxr, xl, xr, xm, x, xcen
    real(8) :: weight

    ! domain = [-0.5 , 0.5]
    bnd = (/-0.5d0, 0.5d0/)
    xcen = 0.5d0 * sum(bnd) 
    select case (exper)
    case (1)
    dxl = 0.005d0
    dxr = 0.01d0
    ngl = 6
    ngr = 6
    nl = int((xcen - bnd(1)) / dxl) + ngl
    xl =  bnd(1) - (ngl - 0.5d0) * dxl
    mass = dxl

    xm = xl + nl * dxl
    nr = int((bnd(2) - xm)/dxr) + ngr
    np = nl + nr

    allocate (parts(np))
    mass = dxl

    ! left hand side
    do 10 i = 1, nl
        x = xl + (i-1)*dxl
        if (x .ge. 0.d0 ) then
            print *, 'Cry wolf, left side too long'
        endif
        parts(i)%x = x
        if ( x .lt. bnd(1) ) then
            parts(i)%ghost = .true.
        else
            parts(i)%ghost = .false.
        endif
        parts(i)%h = dxr
        parts(i)%p = 1.d0
        parts(i)%u = 0.d0
        parts(i)%uold = 0.d0
    10 continue

    ! right hand side
    xm = x
    do 20 i = 1, nr
        x = xm + i*dxr
        if ( x .lt. 0.d0 ) then
            print *, 'Errors in setting RHS, starting before 0.'
        endif
        j = nl + i
        parts(j)%x = x
        if ( x .gt. bnd(2) ) then
            parts(j)%ghost = .true.
        else
            parts(j)%ghost = .false.
        endif
        parts(j)%h = dxr
        parts(j)%p = 0.2
        parts(j)%u = 0.d0
        parts(j)%uold = 0.d0
    20 continue

    case (2)
    dxl = 0.01d0
    ngl = 6
    ngr = 6
    xl =  bnd(1) - (ngl - 0.5d0) * dxl
    mass = dxl

    np = ((bnd(2) - bnd(1)) / dxl) + ngl + ngr
    allocate (parts(np))
    do 40 i = 1, np
        x = xl + (i - 1) * dxl
        parts(i)%x = x
        if ( x .lt. bnd(1) ) then
            parts(i)%ghost = .true.
        elseif ( x .gt. bnd(2) ) then
            parts(i)%ghost = .true.
        else
            parts(i)%ghost = .false.
        endif
        parts(i)%h = dxl
        parts(i)%p = 0.d0
        parts(i)%d = 0.d0
        parts(i)%uold = 0.d0
        parts(i)%u = mass * weight (x, 0.d0, 2.d0 * dxl)
    40 continue
    end select

end subroutine initialize
