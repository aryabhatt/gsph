!************************************************************
! copyright (C) | C_Year| |C_Authorname| (|C_Email|)
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License
! as published by the Free Software Foundation; either version 2
! of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
!***********************************************************

subroutine ene_update (dt)
    use particles
    use properties

    implicit none
    real(8) :: dt

    integer :: i, j, k
    real(8) :: xi, xj, hi, hj, voli, volj
    real(8) :: a, b, vijhi, vijhj, s
    real(8) :: uleft(3), uright(3)
    real(8) :: ustar, pstar
    real(8) :: dwi, dwj, dwdx, rhs
    real(8) :: udot

    do 110 i = 1, np
    if (.not.parts(i)%ghost) then

    rhs = 0.d0
    do 100 j = 1, np
    if (i.eq.j) cycle

    xi = parts(i)%x
    xj = parts(j)%x
    hi = parts(i)%h
    hj = parts(j)%h

    if (abs(xi - xj) .le. 4.25d0 * hi) then
    voli = 1.d0 / parts(i)%d
    volj = 1.d0 / parts(j)%d
            
    ! polynomial density
    a = (voli - volj) / (xi - xj)
    b = (voli + volj) / 2.d0
                
    ! 1 / (density)^2
    vijhi = 0.25d0 * (hi*a)**2 + b**2
    vijhj = 0.25d0 * (hj*a)**2 + b**2

    ! interface position for for RP
    s = (0.5d0 * hi**2 * a * b)/vijhi

    ! left - side
    uleft(1) = parts(j)%d
    uleft(2) = parts(j)%uold
    uleft(3) = parts(j)%p

    ! right - side
    uright(1) = parts(i)%d
    uright(2) = parts(i)%uold
    uright(3) = parts(i)%p

    ! setup riemann problem
    call muscl_projection (parts(j), parts(i), uleft, uright, s, dt, order)
    call riemann_solver (uleft, uright, ustar, pstar)

    dwi = dwdx (xi, xj, sqrt2*hi)
    dwj = dwdx (xi, xj, sqrt2*hj)
    udot = 0.5d0 * (parts(i)%uold + parts(i)%u)
    rhs = rhs - mass*pstar*(ustar - udot) * ((vijhi*dwi + vijhj*dwj))
    endif
    100 continue
    parts(i)%e = parts(i)%e + dt * rhs
    endif
    110 continue
end subroutine ene_update
