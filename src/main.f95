!************************************************************
! copyright (C) 2014 Dinesh Kumar (dkumar@buffalo.edu)
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License
! as published by the Free Software Foundation; either version 2
! of the License, or (at your option) any later version.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with this program; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
!***********************************************************

program godunov_sph
    use particles
    use properties
    use thermodynamics

    implicit none
    real(8) :: xl, xr, xm, x
    real(8) :: time, timestep, dt, maxtime
    real(8) :: writenow, io_interval
    integer :: nl, nr, i, j, k, ngl, ngr
    integer :: iter, maxiter, iwrite
    

    ! set ideal gas index
    call set_gas_index (1.4d0)

    ! set some parameters
    order = 2
    pconst = 1.d0
    maxiter = 1000
    maxtime = 1.d0
    io_interval = 0.1d0

    call initialize (2)
    call density_update 
    call update_ghost_density 

    ! --debug
    !goto 701

    ! initialize energy
    do 30 i = 1, np
    if (.not.parts(i)%ghost) then
    parts(i)%e = parts(i)% p / parts(i)%d / (g - 1.d0)
    endif
    30 continue

    time = 0.d0
    iter = 1
    iwrite = 1
    writenow = 0.1d0
    do while (time.lt.maxtime.and.iter.lt.maxiter) 
    call adapt_smlen 
    call update_ghost_smlen 
    dt = timestep ()

    ! Update Pressure
    call pressure_update
    !call update_ghost_pres

    ! Gradients 
    call calc_gradients 

    ! Momentum equation
    call vel_update (dt)
    call update_ghost_vel

    ! Enerty equation
    !call ene_update (dt)

    ! Density update
    call move_particles (dt)
    call density_update 
    call update_ghost_density

    time = time + dt
    iter = iter + 1
    print *, iter, '-->', time
    if ( time .ge. writenow ) then
        call write_output ( iwrite )
        iwrite = iwrite + 1
        writenow = writenow + io_interval
    endif
        
    end do ! end time loop

    ! --debug
    !701 call write_output (0)

    deallocate (parts)
    ! formats
    900 format(I5', 'F10.5', 'F10.5', 'F10.5)
end program godunov_sph
