module thermodynamics
    implicit none
    real(8) :: g, g1, g2, g3, g4, g5, g6, g7, g8

    contains

    ! Ideal gas index = (Cp / Cv)
    subroutine set_gas_index (gin)
        real(8) :: gin
        g = gin
        g1 = g - 1.d0
        g2 = g / (g - 1.d0)
        g3 = (g + 1.d0) / (2.d0 * g)
        g4 = (g - 1.d0) / (2.d0 * g)
        g5 = (2.d0 * g) / (g - 1.d0)
        g6 = 1.d0 / g
        g7 = 2.d0 / (g - 1.d0)
        g8 = (g - 1.d0) / (g + 1.d0)
    end subroutine set_gas_index

    ! calculate speed of sound in the medium
    function celerity (d, p)
        implicit none
        real(8), intent(in) :: d, p
        real(8) :: celerity

        celerity = sqrt(g * p / d)
    end function celerity
end module thermodynamics

module properties
    implicit none
    integer :: order
    real(8) :: mass
    real(8) :: pconst
    real(8) :: bnd(2)
    real(8), parameter :: sqrt2 = 1.4142135623730951
    real(8), parameter :: inv_sqrt_pi = 0.56418958354775628
end module properties


module particles
    implicit none
    type :: particle
        real(8) :: x, h, d, u, uold, p, e
        real(8) :: dudx(3)
        logical :: ghost
    end type particle
    integer :: np
    type(particle), allocatable :: parts(:)
end module particles
